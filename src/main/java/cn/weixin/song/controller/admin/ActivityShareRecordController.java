package cn.weixin.song.controller.admin;

import cn.weixin.song.model.ActivityShareRecord;

import com.jcbase.core.auth.anno.RequiresPermissions;
import com.jcbase.core.controller.JCBaseController;
import com.jcbase.core.util.JqGridModelUtils;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;


@RequiresPermissions(value={"/activity/share"})
public class ActivityShareRecordController extends JCBaseController {
    
	
	public void index() {
		this.setAttr("aid",this.getPara("aid"));
		render("index.jsp");
	}
	
	public void getListData() {
		String keyword=this.getPara("keyword");
		Integer aid=this.getParaToInt("aid");
		Page<Record> pageInfo=ActivityShareRecord.dao.getActivityShareRecordPage(this.getPage(), this.getRows(),aid,keyword,this.getOrderbyStr());
		this.renderJson(JqGridModelUtils.toJqGridView(pageInfo)); 
	}
    
}
