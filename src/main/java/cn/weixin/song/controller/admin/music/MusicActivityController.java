package cn.weixin.song.controller.admin.music;

import java.util.Map;

import cn.weixin.song.model.MusicActivity;
import cn.weixin.song.model.MusicActivityPlayRecord;
import cn.weixin.song.model.MusicActivityQuestion;

import com.jcbase.core.auth.anno.RequiresPermissions;
import com.jcbase.core.controller.JCBaseController;
import com.jcbase.core.util.JqGridModelUtils;
import com.jcbase.core.view.InvokeResult;
import com.jfinal.plugin.activerecord.Page;

public class MusicActivityController extends JCBaseController {
	
	 public void topScoreView() {
		 int music_activity_id=this.getParaToInt("aid");
		 Page<Map<String,Object>> page=MusicActivityPlayRecord.dao.getMusicActivityPlayRecordRanksViews(music_activity_id, 1, 10);
		 this.renderPageInfo(page);
     }
	 
	 @RequiresPermissions(value={"/mactivity"})
	 public void index(){
		 render("activity_index.jsp");
	 }
	 
	 @RequiresPermissions(value={"/mactivity"})
	 public void getListData() {
		Page<MusicActivity> pageInfo=MusicActivity.dao.getPage(this.getPage(), this.getRows(),this.getOrderby());
		this.renderJson(JqGridModelUtils.toJqGridView(pageInfo)); 
	 }
	 
	 @RequiresPermissions(value={"/mactivity"})
	 public void music_list(){
		 this.setAttr("aid", this.getParaToInt("aid"));
		 render("activity_music_list.jsp");
	 }
	 
	 @RequiresPermissions(value={"/mactivity"})
	 public void getMusicQuestionListData() {
		Page<MusicActivityQuestion> pageInfo=MusicActivityQuestion.dao.getMusicQuestionListData(this.getParaToInt("aid"),this.getPage(), this.getRows(),this.getOrderby());
		this.renderJson(JqGridModelUtils.toJqGridView(pageInfo)); 
	 }
	 
	 @RequiresPermissions(value={"/mactivity"})
	 public void createOrRefreshMusicQuestions() {
		Integer aid=this.getParaToInt("aid"); 
		InvokeResult result=MusicActivityQuestion.dao.createOrRefreshMusicQuestions(aid);
		this.renderJson(result); 
	 }
	 
	 @RequiresPermissions(value={"/mactivity"})
	 public void removeMusic(){
		 Long id=this.getParaToLong("maqId");
		 MusicActivityQuestion.dao.deleteById(id);
		 this.renderJson(InvokeResult.success()); 
	 }
}